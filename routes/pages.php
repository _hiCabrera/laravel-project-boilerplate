<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web page routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function() {
    // return view('welcome');
    return view('design-components');
});

Route::group(['middleware' => ['guest']], function () {

    Route::name('auth.')->namespace('Auth')->group(function () {
        Route::name('login')->get('/login', 'LoginController@index');

        Route::name('reset-password.index')->get('/forgot-password', 'PasswordResetsController@index');
        Route::name('rest-password.reset')->get('/rest-password', 'PasswordResetsController@reset');
    });

});

Route::middleware(['auth'])->group(function () {

    /**
     * Laravel Log Viewer
     */
    Route::name('logs.index')->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

});