<?php

/*
|--------------------------------------------------------------------------
| Web API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web api routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => ['guest']], function () {

    Route::name('auth.')->namespace('Auth')->group(function () {
        Route::name('login')->post('login', 'LoginController@login');

        Route::name('reset-password.send-link')->post('reset-password/send-link', 'PasswordResetsController@sendResetLink');
        Route::name('rest-password.reset')->post('rest-password/reset', 'PasswordResetsController@reset');
    });

    Route::name('users.index')->get('users/', 'Users\UsersController@index');

});