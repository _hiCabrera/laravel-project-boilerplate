<?php
namespace App\Services\Users\Auth;

use App\Mail\Users\ResetPasswordMail;
use App\Models\Users\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Mail;

class PasswordResetter {

    protected $user;

    /**
     * Create new instance of object
     *
     * @author Ian C. Cabrera
     * @param
     */
    public function __construct(User $user = null) {
        if ($user) {
            $this->setUser($user);
        }
    }

    /**
     * Set the user to be reset
     *
     * @author Ian C. Cabrera
     * @param  \App\Models\Users\User $user
     * @return self
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Returns the user to be reset
     *
     * @author Ian C. Cabrera
     * @return \App\Models\Users\User $user
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Sends the reset link to the user's email
     *
     * @author Ian C. Cabrera
     * @return void
     */
    public function sendResetLink(String $email) {
        $passwordReset = PasswordReset::create([
            'email'      => $email,
            'token'      => bcrypt($email),
            'expired_at' => $this->generateExpirationDate(),
            'created_at' => now()->setTimezone('Asia/Manila'),
        ]);

        Mail::to($passwordReset->email)->send(new ResetPasswordMail($passwordReset));
    }

    /**
     * Generates an expiration date for the reset link
     *
     * @author Ian C. Cabrera
     * @return \Carbon\Carbon
     */
    protected function generateExpirationDate() {
        return now()->setTimezone('Asia/Manila')
            ->addMinutes(intval(config('auth.passwords.users.expire')));
    }

    /**
     * Resets the user's password
     *
     * @author Ian C. Cabrera
     * @param  String $token
     * @param  String $password
     * @return self
     */
    public function reset(String $token, String $password) {
        $passwordReset = PasswordReset::whereToken($token)->firstOrFail();
        $this->user    = User::whereEmail($passwordReset->email)->first();
        $this->user    = $this->user->updatePassword($password);

        return $this;
    }

    /**
     * Deletes all the password reset tokens and links in the database
     * of the user
     *
     * @author Ian C. Cabrera
     * @return self
     */ 
    public function clearAllResetLinks() {
        PasswordReset::whereEmail($this->user->email)->delete();

        return $this;
    }

}
