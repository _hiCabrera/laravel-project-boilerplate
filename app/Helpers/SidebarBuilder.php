<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class SidebarBuilder {

    protected $items;

    
    public function __construct() {
        // $this->roles = Auth::user()->activeRoles();
    }

    /**
     * Set's the item configurations to be used in building the sidebar
     * 
     * @author Ian C. Cabrera
     * @param  array $items
     * @return void
     */
    public function setMenuItems(array $items): void {
        $this->items = $items;
    }

    /**
     * Builds the heading banner of the sidebar
     * 
     * @author Ian C. Cabrera
     * @param  String $text
     * @return String
     */
    protected function buildHeading(String $text): String {
        return sprintf('<h6 class="navbar-heading text-muted">%s</h6>', $text);
    }

    /**
     * Builds the sidebar's DOM
     * 
     * @author Ian C. Cabrera
     * @return String
     */
    public function build(): String {
        return sprintf('<sidebar>%s</sidebar>', $this->buildList($this->items));
    }

    /**
     * Builds the sidebar item list's DOM
     * 
     * @author Ian C. Cabrera
     * @param  array $items
     * @param  bool $nested
     * @param  bool $expanded
     * @return String
     */
    protected function buildList(array $items, bool $nested = false, bool $expanded = false): String {
        return sprintf(
            '<ul class="navbar-nav %s" %s>%s</ul>',
            $nested ? 'sub-list' : '',
            $nested ? (!$expanded ? 'style="display: none;"' : '') : '',
            $this->buildListItems($items)
        );
    }

    /**
     * Checks if the user has permission to access the sidebar item
     * 
     * @author Ian C. Cabrera
     * @param  array $permissions
     * @return bool
     */
    public function hasPermission(array $permissions): bool {
        // return count(array_intersect($permissions, $this->roles)) > 0;
        return true;
    }

    /**
     * Builds the list item's DOM
     * 
     * @author Ian C. Cabrera
     * @param  array $items
     * @return String
     */
    protected function buildListItems(array $items): String {
        $listItems = "";
        foreach ($items as $item) {
            if (!$this->hasPermission($item['permission'])) {
                continue;
            }
            $listItems .= $this->buildListItem((object) $item);
        }

        return $listItems;
    }

    /**
     * Prepares the route link
     * 
     * @author Ian C. Cabrera
     * @param  object $route
     * @return String 
     */
    protected function getLink(object $route): String {
        return property_exists($route, 'name') ? route($route->name, $route->params) : '#';
    }

    /**
     * Builds the list item's DOM
     * 
     * @author Ian C. Cabrera
     * @param  object $item
     * @return String
     */
    protected function buildListItem(object $item): String {
        $hasSubItems    = $this->hasSubItems($item);
        $hasActiveItems = $this->hasActiveItem($item);

        return sprintf(
            '<sidebar-item route="%s" icon="%s" title="%s" :nested.boolean="%s" :active.boolean="%s" :expanded.boolean="%s">%s</sidebar-item>',
            $hasSubItems ? '#' : $this->getLink((object) $item->route),
            $item->icon,
            $item->title,
            $hasSubItems ? 'true' : 'false',
            var_export($this->isItemActive($item), true),
            var_export($hasActiveItems, true),
            ($hasSubItems ? $this->buildList($item->items, true, $hasActiveItems) : '')
        );
    }

    /**
     * Builds the list item's DOM
     * 
     * @author Ian C. Cabrera
     * @param  object $item
     * @return String
     */
    protected function hasSubItems(object $item) {
        return isset($item->items) && !empty($item->items);
    }

    /**
     * Checks if the item has an active subitem
     * 
     * @author Ian C. Cabrera
     * @param  object $item
     * @return bool
     */
    protected function hasActiveItem(object $item): bool {
        return $this->hasSubItems($item)
        && array_filter($item->items, function ($subItem) {
            $subItem = (object) $subItem;
            return $this->isItemActive($subItem) || (
                $this->hasSubItems($subItem) &&
                $this->hasActiveItem($subItem)
            );
        });
    }

    /**
     * Checks if item is active
     * 
     * @author Ian C. Cabrera
     * @param  object $item
     * @return bool
     */
    protected function isItemActive($item): bool {
        $route = $this->getLink((object) $item->route);
        if ($route == '#') {
            return false;
        }
        $path = ltrim(str_replace(url('/'), '', $route), '/');

        return Request::is($path, $path . '/*');
    }

}