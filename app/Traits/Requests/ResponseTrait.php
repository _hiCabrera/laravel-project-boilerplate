<?php

namespace App\Traits\Requests;

use Illuminate\Support\Facades\DB;

trait ResponseTrait {

    /**
     * Resolves the response for successful API calls
     *
     * @author Ian C. Cabrera
     * @param  String $message
     * @param  Array $data
     * @param  int $statusCode
     * @return \Illuminate\Http\Response
     */
    protected function resolve(String $message, Array $data, int $statusCode = 200) {
        DB::commit();
        $response = $this->formatResponse(
            $this->resolveMessage($message),
            $data, $statusCode
        );

        return response()->json($response, $statusCode);
    }

    /**
     * Resolves the response for failed API calls
     *
     * @author Ian C. Cabrera
     * @param  String $errorMessage
     * @param  Array|null $data
     * @param  int $statusCode
     * @return \Illuminate\Http\Response
     */
    protected function reject(String $errorMessage, ?Array $data = null, $statusCode = 404) {
        DB::rollBack();
        $response = $this->formatResponse(
            $this->resolveMessage($errorMessage, false),
            $data, $statusCode
        );

        return response()->json($response, $statusCode);
    }

    /**
     * Resolves the message to be returned in the response
     *
     * @author Ian C. Cabrera
     * @param  mixed $message
     * @param  bool $isSuccess
     * @return array
     */
    private function resolveMessage($message, bool $isSuccess = true) {
        return gettype($message) === "string"
        ? [
            'title'   => $isSuccess ? "Success" : "Something went wrong",
            'message' => $message,
        ]
        : $message;
    }

    /**
     * Formats the response
     *
     * @author Ian C. Cabrera
     * @param  mixed $message
     * @param  array $data
     * @param  int $statusCode
     * @return array
     */
    private function formatResponse($message, $data, int $statusCode) {
        return [
            'message'    => $message,
            'result'     => $data,
            'statusCode' => $statusCode,
        ];
    }

}