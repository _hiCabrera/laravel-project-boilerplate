<?php
namespace App\Traits\Users;

use App\Models\Addresses\Address;

trait Resider {

    /**
     * Relationship of user and address
     * 
     * @author Ian C. Cabrera
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address() {
        return $this->morphOne(Address::class, 'resider');
    }

    /**
     * Sets the address of the user
     * 
     * @author Ian C. Cabrera
     * @param  \Array $data
     * @return \App\Models\Addresses\Address
     */
    public function setAddress(Array $data) {
        $address = new Address($data);

        return $this->address()->save($address);
    }

}