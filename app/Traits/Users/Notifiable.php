<?php
namespace App\Traits\Users;

use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Notification;

trait Notifiable {

    public function notifications() {
        return $this->hasMany(Notification::class, 'notified_id');
    }

}