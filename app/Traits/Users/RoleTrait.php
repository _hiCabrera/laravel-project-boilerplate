<?php

namespace App\Traits\Users;

use App\Scopes\RoleScope;

trait RoleTrait {

    protected static function bootRoleTrait() {
        if (!property_exists(static::class, 'roleId') && !isset(static::$roleId)) {
            throw new \Exception("Static property roleID is required", 1);
        }
        static::addGlobalScope(new RoleScope(static::$roleId));
    }

}
