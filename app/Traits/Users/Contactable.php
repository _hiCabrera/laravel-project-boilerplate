<?php
namespace App\Traits\Users;

use Carbon\Carbon;
use App\Models\Users\UserContact;

trait Contactable {

    /**
     * Relationship of user and contacts
     * 
     * @author Ian C. Cabrera
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts() {
        return $this->hasMany(UserContact::class, 'user_id');
    }

    /**
     * Create a new instance of the contact with the associated User.
     *
     * @param  array  $data
     * @return \App\Models\Users\UserContact
     */
    public function addContact(array $data): UserContact {
        return $this->contacts()->create($data);
    }

    /**
     * Create an array of new instances of the related models.
     *
     * @author Ian C. Cabrera
     * @param  array  $records
     * @param  array  $joinings
     * @return array
     */
    public function addContacts(Array $data) {
        return $this->contacts()->createMany($data);
    }

}