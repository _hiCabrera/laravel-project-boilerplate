<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model {
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_id', 'device_id', 'device_model'
    ];

    /**
     * Query scope for devices of a certain user
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  int $user_id
     * @return \Illuminate\Database\Eloquent\Builder 
     */
    public function scopeOfUser($query, int $user_id) {
        return $query->where(compact('user_id'));
    }

    /**
     * Query scope for devices that belongs to a user
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder 
     */
    public function scopeHasOwner($query) {
        return $query->whereNotNull('user_id');
    }

    /**
     * Query scope for contacts that has player_id
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder 
     */
    public function scopeHasPlayerId($query) {
        return $query->whereNotNull('player_id');
    }

    /**
     * Updates the user_id column
     * 
     * @author Ian C. Cabrera
     * @param  int $user_id
     * @return bool
     */
    public function updateUser(int $user_id) {
        $this->user_id = $user_id;

        return $this->update(compact('user_id'));
    }

}
