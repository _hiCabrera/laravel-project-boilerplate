<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model {
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'token', 'created_at', 'expired_at'];

    protected $dates = ['expired_at'];

    public $timestamps = false;


    /**
     * Query scope for active password resets
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query) {
        return $query->where('expired_at', '>', now());
    }

    /**
     * Checks if the password reset is expired
     * 
     * @author Ian C. Cabrera
     * @return bool
     */
    public function isExpired(): bool {
        return $this->expired_at->lt(now()->setTimezone('Asia/Manila'));
    }

}
