<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserContact extends Model {
    
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'number', 'user_id',
        'created_at', 'updated_at'
    ];

    /**
     * Query scope for contacts of a certain user
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  int $user_id
     * @return \Illuminate\Database\Eloquent\Builder 
     */
    public function scopeOfUser($query, int $user_id) {
        return $query->where(compact('user_id'));
    }

}
