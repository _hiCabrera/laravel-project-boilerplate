<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model {
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'email', 'code', 'verified_at',
        'expire_at', 'created_at', 'updated_at',
    ];

    public $dates = [
        'expire_at', 'created_at', 'updated_at',
    ];

    /**
     * Query scope for active user verifications
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder 
     */
    public function scopeActive($query) {
        return $query->where('expire_at', '>', now());
    }


}
