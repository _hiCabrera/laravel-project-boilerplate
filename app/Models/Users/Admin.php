<?php

namespace App\Models\Users;

use App\Traits\Users\RoleTrait;
use Illuminate\Database\Eloquent\Model;

class Admin extends User {

    use RoleTrait;

    public static $codePrefix = 'ADMN_';

    public static $roleId = 1;

    public function getMorphClass() {
        return 'Admin';
    }
    
}
