<?php

namespace App\Models\Addresses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model {
    use SoftDeletes;

    protected $table = 'addresses';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'resider_id', 'resider_type',
        'unit_no', 'street', 'barangay', 'town', 'province', 'country', 'zipcode',
        'lat', 'lng', 'created_at', 'updated_at', 'deleted_at'
    ];


    
    /**
     * Get the class name for polymorphic relations.
     *
     * @author Ian C. Cabrera
     * @return string
     */
    public function getMorphClass() {
        return 'Address';
    }
}
