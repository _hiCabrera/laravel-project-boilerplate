<?php

namespace App\Rules\Users;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class MustHaveLegalAge implements Rule {

    protected $legalAge = 18;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($user = null) {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @author Ian C. Cabrera
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        $birthdate = Carbon::parse($value);
        if ($this->user != null && $this->user->isAdmin()) {
            return true;
        }

        return $this->legalAge <= $birthdate->age;
    }

    /**
     * Get the validation error message.
     *
     * @author Ian C. Cabrera
     * @return string
     */
    public function message() {
        return sprintf("Must be atleast %s years old.", $this->legalAge);
    }
}
