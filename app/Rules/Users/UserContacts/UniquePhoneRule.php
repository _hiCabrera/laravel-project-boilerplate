<?php

namespace App\Rules\Users\UserContacts;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Users\User;

class UniquePhoneRule implements Rule {

    protected $userId;
    protected $column;

    /**
     * Create a new rule instance.
     *
     * @author Ian C. Cabrera
     * @return void
     */
    public function __construct(int $userId, String $column, int $exceptId = 0) {
        $this->userId = $userId;
        $this->column = $column;
        $this->exceptId = $exceptId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @author Ian C. Cabrera
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        $column = $this->column;
        $exceptId = $this->exceptId;
        $exists = User::whereHas(
            'contacts',
            function($contacts) use ($column, $value, $exceptId) {
                $contacts->where($column, $value)
                    ->where('id', '!=', $exceptId);
            })
            ->where('id', $this->userId)->exists();
            
        return !$exists;
    }

    /**
     * Get the validation error message.
     *
     * @author Ian C. Cabrera
     * @return string
     */
    public function message() {
        return 'The contact :attribute already exists for the user';
    }
}
