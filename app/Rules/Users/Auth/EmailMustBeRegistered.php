<?php

namespace App\Rules\Users\Auth;

use App\Models\Users\User;
use Illuminate\Contracts\Validation\Rule;

class EmailMustBeRegistered implements Rule {

    /**
     * Determine if the validation rule passes.
     *
     * @author Ian C. Cabrera
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        return User::whereEmail($value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @author Ian C. Cabrera
     * @return string
     */
    public function message() {
        return 'The email does not exists in our records.';
    }
}
