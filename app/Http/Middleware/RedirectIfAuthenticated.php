<?php

namespace App\Http\Middleware;

use Closure;
use Throwable;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {


    /**
     * Handle an incoming request.
     *
     * @author Ian C. Cabrera
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->check()) {
            return $this->getRedirectionLink();
        }

        return $next($request);
    }


    /**
     * Generates the link that the user will be redirected to
     * upon login 
     * 
     * @author Ian C. Cabrera
     * @return mixed
     */
    public function getRedirectionLink() {
        try {
            $route = request()->has('rn')
                ? route(request()->rn, request()->rp)
                : route('pages.dashboard.index');
        } catch (Throwable $th) {
            $route = route('pages.dashboard.index');
        }

        return redirect($route);
    }
}
