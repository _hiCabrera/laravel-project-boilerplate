<?php

namespace App\Http\Controllers\Pages\Auth;

use Exception;
use Illuminate\Http\Request;
use App\Models\Users\PasswordReset;
use App\Http\Controllers\Controller;

/**ss
 * PasswordResetsController class
 * 
 */
class PasswordResetsController extends Controller {

    public function __construct() {
        $this->viewPath = 'pages.common.password-reset';
    }

    /**
     * Shows the application's request password reset token page 
     * 
     * @author Ian C. Cabrera
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->view('index');
    }

    

    /**
     * Shows the application's reset password page
     * 
     * @author Ian C. Cabrera
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request) {
        if (!$request->has('token')) {
            redirect('pages.login');
        }

        $passwordReset = PasswordReset::where($request->only('token'))->first();
        if ($passwordReset === null) {
            throw new Exception('Given token is invalid');
        
        } else if ($passwordReset->isExpired()) {
            throw new Exception('Password reset link is expired');
        }

        return $this->view('reset', compact('passwordReset'));
    }

}
