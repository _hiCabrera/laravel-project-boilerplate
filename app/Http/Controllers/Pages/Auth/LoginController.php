<?php

namespace App\Http\Controllers\Pages\Auth;

use App\Http\Controllers\Controller;

class LoginController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
        $this->viewPath = 'pages.auth.login';
    }

    /**
     * Shows the application's login page 
     * 
     * @author Ian C. Cabrera
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->view('index');
    }

}
