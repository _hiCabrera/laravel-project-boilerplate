<?php

namespace App\Http\Controllers\API\Backend\Auth;

use Exception;
use App\Http\Controllers\Controller;
use App\Services\Users\Auth\PasswordResetter;
use App\Http\Requests\Backend\Auth\ForgotPasswordRequest;

class PasswordResetsController extends Controller {


    /**
     * Sends the reset link to the user's emaail
     * 
     * @author Ian C. Cabrera
     * @param  \App\Http\Requests\Backend\Auth\ForgotPasswordRequest $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLink(ForgotPasswordRequest $request) {
        try {
            (new PasswordResetter)->sendResetLink($request->email);

            return $this->resolve('res.auth.send-reset-link.success');
        } catch (Exception $ex) {
            return $this->reject($ex, 'res.auth.send-reset-link.failed');
        }
    }

    /**
     * 
     * @author Ian C. Cabrera
     * @param  \App\Http\Requests\Backend\Auth\ForgotPasswordRequest $request
     * @return \Illuminate\Http\Response
     */
    public function reset(ForgotPasswordRequest $request) {
        try {
            $resetter = new PasswordResetter;
            $resetter->reset($request->token, $request->password)
                ->clearAllResetLinks();
        
            return $this->resolve('res.auth.password-reset.success', [
                'email' => $resetter->getUser()->email
            ]);
        } catch (Exception $ex) {
            return $this->reject($ex, 'res.auth.password-reset.failed');
        }
    }
    
}
