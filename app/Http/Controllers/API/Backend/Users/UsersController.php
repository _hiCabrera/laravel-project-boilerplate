<?php

namespace App\Http\Controllers\API\Backend\Users;

use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller {
    

    public function index() {
        $users = User::paginate(10);

        return $this->resolve('res.fetch.success', compact('users'));
    }

}
