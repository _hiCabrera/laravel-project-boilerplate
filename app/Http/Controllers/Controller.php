<?php

namespace App\Http\Controllers;

use App\Traits\Requests\ResponseTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Controller class
 */
class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ResponseTrait;

    /**
     * Returns the view based on the stated view path attribute
     * 
     * @param String $path
     * @param Array|null $data
     * 
     * @return view
     */
    protected function view(String $path, ?Array $data = null) {
        $path = $this->resolvePath($path);

        return $data ? view($path)->with($data) : view($path);
    }

    /**
     * Resovles the givn path based on the stated view path attribute
     * 
     * @param String $path
     * 
     * @return String
     */
    protected function resolvePath(String $path) {
        $viewPath = property_exists($this, 'viewPath')
            ? $this->viewPath . '.' : "";

        return $viewPath . $path;
    }


}
