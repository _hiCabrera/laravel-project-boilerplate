<?php

namespace App\Http\Requests\Backend\Auth;

use App\Http\Requests\RestFormRequest;
use App\Rules\Users\Auth\EmailMustBeRegistered;

class ForgotPasswordRequest extends RestFormRequest {

    /**
     * Returns the rules for POST requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPostRules(): Array{
        return [
            'password' => ['required', 'min:5', 'confirmed'],
        ];
    }

    /**
     * Returns the rules for PUT requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPutRules(): Array{
        return [
            'email'   => ['required', 'email', new EmailMustBeRegistered],
        ];
    }

}