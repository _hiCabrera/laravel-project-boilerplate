<?php
namespace App\Http\Requests\Adapters;

interface RequestAdapter {

    /**
     * Transforms the array to a certain form
     * 
     * @param \Array $data
     * @return \Array
     */
    public function format(Array $data): Array;

}