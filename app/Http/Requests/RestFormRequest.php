<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class RestFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return false;
    }

    /**
     * Returns the validation rules per request type
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    public function getRules(): Array{
        switch (request()->method()) {
            case 'POST':
                return $this->getPostRules();
            case 'PUT':
                return $this->getPutRules();
            case 'PATCH':
                return $this->getPatchRules();
            case 'DELETE':
                return $this->getDeleteRules();
            default:
                return [];
        }
    }

    /**
     * Returns the rules for POST requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPostRules(): Array {
        return [];
    }

    /**
     * Returns the rules for PUT requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPutRules(): Array {
        return [];
    }
    
    /**
     * Returns the rules for PATCH requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPatchRules(): Array {
        return [];
    }

    /**
     * Returns the rules for DELETE requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getDeleteRules(): Array {
        return [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @author Ian C. Cabrera
     * @return array
     */
    public function rules() {
        return $this->getRules();
    }

    /**
     * Gets the data needed per request type based on
     * the given keys for it's corresponding request type
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    public function getFormData(): Array {
        return $this->only($this->getFormKeys());
    }

    /**
     * Gets the indexes of the data to be returned by getFormData
     * based on the request type
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    public function getFormKeys(): Array {
        switch (request()->method()) {
            case 'POST':
                return $this->getPostKeys();
            case 'PUT':
                return $this->getPutKeys();
            case 'PATCH':
                return $this->getPatchKeys();
            case 'DELETE':
                return $this->getDeleteKeys();
            default:
                return [];
        }
    }

    /**
     * Gets the indexes of the data to be returned by getFormData
     * for POST requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPostKeys(): Array {
        return [];
    }

    /**
     * Gets the indexes of the data to be returned by getFormData
     * for PUT requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPutKeys(): Array {
        return [];
    }

    /**
     * Gets the indexes of the data to be returned by getFormData
     * for PATCH requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getPatchKeys(): Array {
        return [];
    }

    /**
     * Gets the indexes of the data to be returned by getFormData
     * for DELETE requests
     * 
     * @author Ian C. Cabrera
     * @return array
     */
    protected function getDeleteKeys(): Array {
        return [];
    }
}
