<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BindingServiceProvider extends ServiceProvider {

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->registerBindings();
    }

    /**
     * Register the dependencies in the binding attribute
     * 
     * @author Ian C. Cabrera
     * @return void 
     */
    public function registerBindings() {
        foreach ($this->bindings as $request => $requester) {
            $this->bindDependency($request, $requester);
        }
    }

    /**
     * Registers the bindings for the given dependency
     * 
     * @author Ian C. Cabrera
     * @return void
     */
    protected function bindDependency(String $dependency, Array $bindings) {
        foreach ($bindings as $dependent => $service) {
            $this->app
                ->when($dependent)
                ->needs($dependency)
                ->give($service);
        }
    }
}
