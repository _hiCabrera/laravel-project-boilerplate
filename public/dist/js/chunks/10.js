webpackJsonp([10],{

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(343)
/* template */
var __vue_template__ = __webpack_require__(344)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/misc/Slider.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-39b8e089", Component.options)
  } else {
    hotAPI.reload("data-v-39b8e089", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Slider",
  computed: {
    id: function id() {
      return {
        container: 'input-slider-' + this._uid + '-container',
        component: 'input-slider-' + this._uid,
        value: 'input-slider-' + this._uid + '-value'
      };
    }
  },
  data: function data() {
    return {
      sliderValue: this.value
    };
  },
  methods: {
    initialize: function initialize() {
      var _this = this;

      noUiSlider.create(this.$refs.slider, {
        start: this.sliderValue || 0,
        step: 5,
        connect: 'lower',
        tooltips: [true],
        range: {
          'min': this.min,
          'max': this.max
        }
      });
      this.$refs.slider.noUiSlider.on('change', function (values) {
        return _this.onChange(values);
      });

      if (this.disabled) {
        this.$refs.slider.setAttribute('disabled', true);
      }
    },
    onChange: function onChange(values) {
      this.$emit('input', this.sliderValue = parseInt(values[0]));
    }
  },
  watch: {
    value: function value(newVal, oldVal) {
      if (oldVal === undefined) {
        this.$refs.slider.noUiSlider.set(newVal);
      } else if (newVal !== this.sliderValue) {
        this.$refs.slider.noUiSlider.set(this.sliderValue = newVal);
      }
    },
    disabled: function disabled(newValue) {
      newValue ? this.$refs.slider.setAttribute('disabled', true) : this.$refs.slider.removeAttribute('disabled');
    }
  },
  props: {
    min: 0,
    max: 100,
    value: Number,
    disabled: false
  },
  mounted: function mounted() {
    this.initialize();
  }
});

/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "input-slider-container", attrs: { id: _vm.id.container } },
    [
      _c("div", {
        ref: "slider",
        staticClass: "input-slider",
        attrs: {
          id: _vm.id.component,
          "data-range-value-min": _vm.min,
          "data-range-value-max": _vm.max
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "row mt-3 d-none" }, [
        _c("div", { staticClass: "col-6" }, [
          _c("span", {
            staticClass: "range-slider-value",
            attrs: { id: _vm.id.value, "data-range-value-low": _vm.min }
          })
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-39b8e089", module.exports)
  }
}

/***/ })

});