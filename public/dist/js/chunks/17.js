webpackJsonp([17],{

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(302)
/* template */
var __vue_template__ = __webpack_require__(303)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/containers/cards/CardInfiniteScrollList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-075b381d", Component.options)
  } else {
    hotAPI.reload("data-v-075b381d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_utils_mixins__ = __webpack_require__(172);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var CardTable = function CardTable() {
  return __webpack_require__.e/* import() */(0/* duplicate */).then(__webpack_require__.bind(null, 271));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CardInfiniteScrollList',
  components: {
    CardTable: CardTable
  },
  computed: {
    inlineStyle: function inlineStyle() {
      return this.maxHeight ? 'max-height:' + this.maxHeight + '; ' : '';
    },
    headerCount: function headerCount() {
      return this.$slots.header ? this.$slots.header.length : 1;
    },
    recordsEmpty: function recordsEmpty() {
      return this.records && this.records.length == 0;
    },
    hasMore: function hasMore() {
      return !!(this.pagination.next_page_url || this.pagination.next);
    },
    styleClass: function styleClass() {
      return {
        scrollTable: {
          'infinite-scroll-table': true,
          'long': this["long"]
        }
      };
    }
  },
  props: {
    title: String,
    fetchLink: String,
    promptFail: {
      "default": true,
      type: Boolean
    },
    paginatePath: {
      type: String,
      "default": ""
    },
    "long": Boolean,
    simple: {
      "default": false,
      type: Boolean
    },
    maxHeight: String
  },
  data: function data() {
    return {
      link: '',
      fetching: true,
      pagination: {
        next_page_url: true
      },
      records: []
    };
  },
  methods: {
    fetchData: function fetchData() {
      var _this = this;

      this.fetching = true;
      axios(this.link).then(function (response) {
        return _this.mapData(response);
      })["catch"](function (error) {
        return _this.fetchFailed(error);
      })["finally"](function () {
        return _this.fetching = false;
      });
    },
    fetchFailed: function fetchFailed(error) {
      this.$emit('fetch-failed', error);

      if (this.promptFail) {
        this.promptErrors('Failed to load table data');
      }

      this.pagination = {
        data: []
      };
    },
    mapData: function mapData(response) {
      var _this2 = this;

      this.pagination = this.getPaginationFromPath(response);
      this.link = this.pagination.next_page_url || this.pagination.next;
      this.pagination.data.forEach(function (item) {
        return _this2.records.push(item);
      });
      this.$emit('recordloaded', this.records);
    },
    getPaginationFromPath: function getPaginationFromPath(data) {
      this.paginatePath.split('.').forEach(function (index) {
        return data = data[index];
      });
      return data;
    },
    isNearToBottom: function isNearToBottom(table) {
      return table.scrollTop + table.clientHeight > table.scrollHeight - 100;
    },
    loadNewData: function loadNewData(event) {
      var table = event.target;

      if (this.fetching || !this.hasMore || !this.isNearToBottom(table)) {
        return;
      }

      this.fetchData();
    },
    movePage: function movePage(event) {
      event.preventDefault();
      this.link = event.target.href;
      this.currentPage = parseInt(event.target.dataset.page);
      this.fetchData();
    },
    refreshTable: function refreshTable() {
      this.link = this.fetchLink;
      this.records = [];
      this.fetchData();
    }
  },
  mixins: [__WEBPACK_IMPORTED_MODULE_0_utils_mixins__["a" /* default */]],
  mounted: function mounted() {
    var _this3 = this;

    setTimeout(function () {
      (_this3.simple ? _this3.$refs.list : _this3.$refs.table.$el.querySelector('.table-responsive')).onscroll = function (e) {
        return _this3.loadNewData(e);
      };

      _this3.refreshTable();

      $(_this3.refreshButton).tooltip();
    }, 1000);
  }
});

/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.simple
    ? _c(
        "div",
        {
          ref: "list",
          class: _vm.styleClass.scrollTable,
          style: _vm.inlineStyle,
          attrs: { id: "list-conatiner" }
        },
        [
          _c(
            "table",
            { staticClass: "table-responsive" },
            [
              !_vm.fetching && _vm.recordsEmpty
                ? _c("tr", [
                    _c("td", { attrs: { colspan: _vm.headerCount } }, [
                      _vm._v(
                        "\n                No record was found\n            "
                      )
                    ])
                  ])
                : _vm._l(this.records, function(record, key) {
                    return _c(
                      "tr",
                      { key: key },
                      [
                        _vm._t("body", null, {
                          record: record,
                          refreshPage: _vm.refreshTable
                        })
                      ],
                      2
                    )
                  }),
              _vm._v(" "),
              _vm.fetching
                ? _c("tr", [
                    _c(
                      "td",
                      { attrs: { colspan: _vm.headerCount } },
                      [
                        _vm._t("loading", [
                          _c("i", {
                            staticClass: "fa fa-spinner fa-spin pr-1"
                          }),
                          _vm._v(
                            "\n                    Loading data, please wait ...\n                "
                          )
                        ])
                      ],
                      2
                    )
                  ])
                : _vm._e()
            ],
            2
          )
        ]
      )
    : _c(
        "card-table",
        {
          ref: "table",
          class: _vm.styleClass.scrollTable,
          attrs: { title: _vm.title, styleClass: _vm.inlineStyle }
        },
        [
          _c(
            "template",
            { slot: "title-buttons" },
            [
              _vm._t("title-buttons"),
              _vm._v(" "),
              _c(
                "button",
                {
                  ref: "refreshButton",
                  staticClass: "btn btn-sm btn-link btn-tooltip",
                  attrs: {
                    "data-toggle": "tooltip",
                    title: "Refresh",
                    "data-original-title": "Refresh"
                  },
                  on: { click: _vm.refreshTable }
                },
                [_c("i", { staticClass: "fa fa-refresh" })]
              )
            ],
            2
          ),
          _vm._v(" "),
          _c("template", { slot: "thead" }, [_vm._t("header")], 2),
          _vm._v(" "),
          _c(
            "template",
            { slot: "tbody" },
            [
              !_vm.fetching && _vm.recordsEmpty
                ? _c("tr", [
                    _c("td", { attrs: { colspan: _vm.headerCount } }, [
                      _vm._v(
                        "\n                No record was found\n            "
                      )
                    ])
                  ])
                : _vm._l(this.records, function(record, key) {
                    return _c(
                      "tr",
                      { key: key },
                      [
                        _vm._t("body", null, {
                          record: record,
                          refreshPage: _vm.refreshTable
                        })
                      ],
                      2
                    )
                  }),
              _vm._v(" "),
              _vm.fetching
                ? _c("tr", [
                    _c("td", { attrs: { colspan: _vm.headerCount } }, [
                      _c("i", { staticClass: "fa fa-spinner fa-spin pr-1" }),
                      _vm._v(
                        "\n                Loading data, please wait ...\n            "
                      )
                    ])
                  ])
                : _vm._e()
            ],
            2
          ),
          _vm._v(" "),
          _c("template", { slot: "footer" }, [_vm._t("footer")], 2)
        ],
        2
      )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-075b381d", module.exports)
  }
}

/***/ })

});