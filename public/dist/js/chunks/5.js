webpackJsonp([5],{

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(322)
/* template */
var __vue_template__ = __webpack_require__(323)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/forms/ExcelUploader.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1f90686c", Component.options)
  } else {
    hotAPI.reload("data-v-1f90686c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Form */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Errors__ = __webpack_require__(292);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Form =
/*#__PURE__*/
function () {
  function Form(data) {
    _classCallCheck(this, Form);

    this.mapData(data);
    this.isRequesting = false;
    this.submitKeys = [];
    this.errors = new __WEBPACK_IMPORTED_MODULE_0__Errors__["a" /* default */]();
  }

  _createClass(Form, [{
    key: "mapData",
    value: function mapData(data) {
      this.originalData = _objectSpread({}, data);
      this.resetData();
    }
  }, {
    key: "reset",
    value: function reset() {
      this.resetData(); // this.clearErrors();
    }
  }, {
    key: "resetData",
    value: function resetData() {
      var _this = this;

      Object.keys(this.originalData).forEach(function (i) {
        return _this[i] = _this.originalData[i];
      });
    }
  }, {
    key: "setSubmitKeys",
    value: function setSubmitKeys(keys) {
      this.submitKeys = Array.isArray(keys) ? keys : [keys];
    }
  }, {
    key: "clearErrors",
    value: function clearErrors() {
      this.errors.clear();
    }
  }, {
    key: "getData",
    value: function getData() {
      var _this2 = this;

      var data = {};
      var keys = this.submitKeys.length > 0 ? this.submitKeys : Object.keys(this.originalData);
      keys.forEach(function (i) {
        return data[i] = _this2[i];
      });
      return data;
    }
  }, {
    key: "getHeaders",
    value: function getHeaders() {
      return {
        'Content-Type': 'multipart/form-data'
      };
    }
  }, {
    key: "getFormData",
    value: function getFormData() {
      var data = this.getData();
      var formData = new FormData();
      Object.keys(data).forEach(function (key) {
        if (!Array.isArray(data[key])) {
          formData.append(key, data[key]);
          return;
        }

        data[key].forEach(function (item) {
          return formData.append(key + '[]', item);
        });
      });
      return formData;
    }
  }, {
    key: "send",
    value: function send(method, url) {
      var _this3 = this;

      var multiPart = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return new Promise(function (resolve, reject) {
        _this3.isRequesting = true;

        _this3.prepareRequest(method, url, multiPart).then(resolve)["catch"](function (error) {
          // this.errors.record(error)
          reject(error);
        })["finally"](function () {
          return _this3.isRequesting = false;
        });
      });
    }
  }, {
    key: "prepareRequest",
    value: function prepareRequest(method, url, multiPart) {
      var data = multiPart ? this.getFormData() : this.getData();
      var headers = this.getHeaders();
      return multiPart ? axios({
        method: method,
        url: url,
        data: data,
        headers: headers
      }) : axios({
        method: method,
        url: url,
        data: data
      });
    }
  }, {
    key: "sendGet",
    value: function sendGet(url) {
      var multiPart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return this.send('get', url);
    }
  }, {
    key: "sendPost",
    value: function sendPost(url) {
      var multiPart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return this.send('post', url, multiPart);
    }
  }, {
    key: "sendPut",
    value: function sendPut(url) {
      var multiPart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return this.send('put', url);
    }
  }, {
    key: "sendPatch",
    value: function sendPatch(url) {
      var multiPart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return this.send('patch', url);
    }
  }, {
    key: "sendDelete",
    value: function sendDelete(url) {
      var multiPart = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return this.send('delete', url);
    }
  }], [{
    key: "fileListToArray",
    value: function fileListToArray(fileList) {
      var files = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = fileList[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var file = _step.value;
          files.push(file);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return files;
    }
  }]);

  return Form;
}();

/* harmony default export */ __webpack_exports__["a"] = (Form);


/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Errors */
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Errors =
/*#__PURE__*/
function () {
  function Errors() {
    _classCallCheck(this, Errors);

    this.clear();
  }

  _createClass(Errors, [{
    key: "has",
    value: function has(field) {
      return this.errors.hasOwnProperty(key);
    }
  }, {
    key: "any",
    value: function any() {
      return !!Object.keys(this.errors).length;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.errors = {};
    }
  }, {
    key: "get",
    value: function get(field) {
      return _typeof(this.errors[field]) === "object" ? this.errors[field][0] : this.errors[field];
    }
  }, {
    key: "getAll",
    value: function getAll(field) {
      return field ? this.errors[field] : this.errors;
    }
  }, {
    key: "record",
    value: function record(errors, field) {
      if (!field && _typeof(errors) !== "object") {
        throw new Error("Error messages must be an array if not given a field");
      }

      if (field) {
        this.errors[field] = _typeof(errors) === "object" ? errors : [errors];
        return;
      }

      this.errors = errors;
    }
  }]);

  return Errors;
}();

/* harmony default export */ __webpack_exports__["a"] = (Errors);


/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_utils_mixins__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_models_components_Form__ = __webpack_require__(291);
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ExcelUploader',
  computed: {
    id: function id() {
      return "excel-uploader-".concat(this._uid);
    },
    domSelector: function domSelector() {
      return "#".concat(this.id);
    }
  },
  data: function data() {
    return {
      link: "#"
    };
  },
  methods: {
    upload: function upload() {
      var _this = this;

      if (!this.$refs.input.files.length) {
        return;
      }

      var form = new __WEBPACK_IMPORTED_MODULE_1_models_components_Form__["a" /* default */]({
        excel: __WEBPACK_IMPORTED_MODULE_1_models_components_Form__["a" /* default */].fileListToArray(this.$refs.input.files)
      });
      form.sendPost(this.link, true).then(function (response) {
        _this.promptSuccess(response.data.message);

        _this.$emit('uploaded', response);
      })["catch"](function (error) {
        _this.promptErrors(error.response.data.message);

        _this.$emit('uploa-failed', error);
      });
    }
  },
  mixins: [__WEBPACK_IMPORTED_MODULE_0_utils_mixins__["a" /* default */]],
  props: {
    url: String,
    text: String
  },
  mounted: function mounted() {
    this.link = this.url || this.link;
  }
});

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", [
    _c(
      "button",
      {
        staticClass: "btn btn-sm btn-primary",
        on: {
          click: function($event) {
            return _vm.$refs.input.click()
          }
        }
      },
      [
        _c("i", { staticClass: "fa fa-upload mr-1" }),
        _vm._v(" "),
        _c("span", { domProps: { textContent: _vm._s(_vm.text) } })
      ]
    ),
    _vm._v(" "),
    _c("input", {
      ref: "input",
      attrs: { hidden: "", type: "file", id: _vm.id },
      on: { change: _vm.upload }
    })
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1f90686c", module.exports)
  }
}

/***/ })

});