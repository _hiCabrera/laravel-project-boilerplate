webpackJsonp([22],{

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(299)
/* template */
var __vue_template__ = __webpack_require__(300)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/containers/cards/Pagination.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bc933470", Component.options)
  } else {
    hotAPI.reload("data-v-bc933470", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Pagination",
  props: {
    maxVisibleButtons: {
      type: Number,
      "default": 5
    },
    totalPages: {
      type: Number,
      required: true,
      "default": 1
    },
    total: {
      type: Number,
      required: true,
      "default": 0
    },
    currentPage: {
      type: Number,
      required: true,
      "default": 1
    },
    activeItemClass: {
      type: String,
      "default": "active"
    },
    pageClasses: {
      type: String,
      "default": ""
    },
    firstClasses: {
      type: String,
      "default": ""
    },
    previousClasses: {
      type: String,
      "default": ""
    },
    nextClasses: {
      type: String,
      "default": ""
    },
    lastClasses: {
      type: String,
      "default": ""
    },
    firstText: {
      type: String,
      "default": "<<"
    },
    previousText: {
      type: String,
      "default": "<"
    },
    nextText: {
      type: String,
      "default": ">"
    },
    lastText: {
      type: String,
      "default": ">>"
    },
    simplified: {
      type: Boolean,
      "default": false
    }
  },
  computed: {
    startPage: function startPage() {
      if (this.totalPages < this.maxVisibleButtons) {
        return 1;
      }

      if (this.currentPage > Math.floor(this.maxVisibleButtons / 2)) {
        if (this.totalPages - Math.floor(this.maxVisibleButtons / 2) < this.currentPage) {
          return this.totalPages - this.maxVisibleButtons + 1;
        }

        return this.currentPage - Math.floor(this.maxVisibleButtons / 2);
      } else {
        return 1;
      }
    },
    pages: function pages() {
      var range = [];

      for (var i = this.startPage; i <= Math.min(this.startPage + this.maxVisibleButtons - 1, this.totalPages); i += 1) {
        range.push({
          name: i,
          isDisabled: i === this.currentPage
        });
      }

      return range;
    },
    isInFirstPage: function isInFirstPage() {
      return this.currentPage === 1;
    },
    isInLastPage: function isInLastPage() {
      return this.currentPage === this.totalPages;
    }
  },
  methods: {
    onClickFirstPage: function onClickFirstPage() {
      this.$emit('pageChanged', 1);
    },
    onClickPreviousPage: function onClickPreviousPage() {
      this.$emit('pageChanged', this.currentPage - 1);
    },
    onClickPage: function onClickPage(page) {
      this.$emit('pageChanged', page);
    },
    onClickNextPage: function onClickNextPage() {
      this.$emit('pageChanged', this.currentPage + 1);
    },
    onClickLastPage: function onClickLastPage() {
      this.$emit('pageChanged', this.totalPages);
    },
    isPageActive: function isPageActive(page) {
      return this.currentPage === page;
    }
  }
});

/***/ }),

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "ul",
    { staticClass: "pagination" },
    [
      !_vm.simplified
        ? _c("li", { staticClass: "page-item" }, [
            _c(
              "a",
              {
                class: [
                  "p-2 page-link",
                  _vm.firstClasses,
                  _vm.isInFirstPage ? "disabled" : ""
                ],
                on: { click: _vm.onClickFirstPage }
              },
              [_c("i", { staticClass: "fas fa-fast-backward" })]
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm._l(_vm.pages, function(page, key) {
        return _c(
          "li",
          {
            key: key,
            class:
              "page-item " +
              (_vm.isPageActive(page.name)
                ? " disabled " + _vm.activeItemClass
                : "")
          },
          [
            _c(
              "a",
              {
                class: ["p-2 page-link", _vm.pageClasses],
                on: {
                  click: function($event) {
                    return _vm.onClickPage(page.name)
                  }
                }
              },
              [_vm._v("\n            " + _vm._s(page.name) + "\n        ")]
            )
          ]
        )
      }),
      _vm._v(" "),
      !_vm.simplified
        ? _c("li", { staticClass: "page-item" }, [
            _c(
              "a",
              {
                class: [
                  "p-2 page-link",
                  _vm.lastClasses,
                  _vm.isInLastPage ? "disabled" : ""
                ],
                on: { click: _vm.onClickLastPage }
              },
              [_c("i", { staticClass: "fas fa-fast-forward" })]
            )
          ])
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-bc933470", module.exports)
  }
}

/***/ })

});