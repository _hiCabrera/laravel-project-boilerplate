webpackJsonp([2],{

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(336)
/* template */
var __vue_template__ = __webpack_require__(340)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/misc/GoogleMap/GoogleMap.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-51b6811f", Component.options)
  } else {
    hotAPI.reload("data-v-51b6811f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Autocomplete__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Autocomplete___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Autocomplete__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "GoogleMap",
  components: {
    Autocomplete: __WEBPACK_IMPORTED_MODULE_0__Autocomplete___default.a
  },
  data: function data() {
    return {
      // default to Montreal to keep it simple
      // change this to whatever makes sense
      center: {
        lat: 45.508,
        lng: -73.587
      },
      markers: [],
      places: [],
      currentPlace: null,
      geocoder: null,
      addressStructure: {
        lat: 0,
        lng: 0,
        barangay: '',
        city: '',
        province: '',
        country: '',
        zipcode: ''
      },
      addressAllocation: {
        barangay: 'neighborhood',
        city: 'locality',
        province: 'administrative_area_level_2',
        country: 'country',
        zipcode: 'postal_code'
      }
    };
  },
  mounted: function mounted() {
    this.center = {
      lat: parseFloat(this.initialLocation.lat || 0),
      lng: parseFloat(this.initialLocation.lng || 0)
    }; // this.geolocate();

    if (this.initialValue) {
      this.markers = this.multiple ? this.initialValue : [this.initialValue];
    }
  },
  methods: {
    getGeocoder: function getGeocoder() {
      return this.geocoder || (this.geocoder = new google.maps.Geocoder());
    },
    loadAddress: function loadAddress(lat, lng) {
      var _this = this;

      lat = lat || 0;
      lng = lng || 0;
      return new Promise(function (resolve, reject) {
        _this.getGeocoder().geocode({
          location: {
            lat: lat,
            lng: lng
          }
        }, function (result, status) {
          if (status != "OK") {
            reject(result, status);
          } else {
            var addresses = _this.filterAddresses(result);

            var address = _this.transformData(addresses[0]);

            resolve(address);
          }
        });
      });
    },
    transformData: function transformData(geocodedAddress) {
      var _this2 = this;

      var address = _objectSpread({}, this.addressStructure, {
        lat: geocodedAddress.geometry.location.lat(),
        lng: geocodedAddress.geometry.location.lng()
      });

      geocodedAddress.address_components.forEach(function (component) {
        for (var key in _this2.addressAllocation) {
          if (component.types.includes(_this2.addressAllocation[key])) {
            address[key] = component.long_name;
            break;
          }
        }
      });
      return address;
    },
    filterAddresses: function filterAddresses(addresses) {
      var filteredAddresses = addresses.filter(function (address) {
        return !address.formatted_address.toLowerCase().includes("unnamed");
      });
      return filteredAddresses.length > 0 ? filteredAddresses : addresses;
    },
    selectMarker: function selectMarker(payload) {
      this.saveMarker(payload.latLng.lat(), payload.latLng.lng());
    },
    saveMarker: function saveMarker(lat, lng) {
      var _this3 = this;

      if (this.disabled) {
        return;
      }

      this.loadAddress(lat, lng).then(function (response) {
        var data = {
          address: response,
          position: {
            lat: lat,
            lng: lng
          }
        };
        _this3.multiple ? _this3.markers.push(data) : _this3.markers = [data];

        _this3.$emit('input', _this3.markers);

        _this3.$emit('change', _this3.markers);
      })["catch"](function (error, status) {
        console.log(error, status);
      });
    },
    // receives a place object via the autocomplete component
    setPlace: function setPlace(place) {
      this.currentPlace = place;
      this.addMarker();
    },
    addMarker: function addMarker() {
      if (!this.currentPlace) {
        return;
      }

      var marker = {
        lat: this.currentPlace.geometry.location.lat(),
        lng: this.currentPlace.geometry.location.lng()
      };
      this.saveMarker(marker.lat, marker.lng);
      this.places.push(this.currentPlace);
      this.center = marker;
      this.currentPlace = null;
    },
    geolocate: function geolocate() {
      var _this4 = this;

      navigator.geolocation.getCurrentPosition(function (position) {
        _this4.center = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
      });
    }
  },
  props: {
    multiple: {
      "default": false,
      type: Boolean
    },
    disabled: {
      type: Boolean,
      "default": false
    },
    initialLocation: {
      type: Object,
      "default": function _default() {
        return {
          lat: window.google_maps_default_lat || 14.5995124,
          lng: window.google_maps_default_lng || 120.9842195
        };
      }
    },
    initialValue: Object | Array
  }
});

/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(338)
/* template */
var __vue_template__ = __webpack_require__(339)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/misc/GoogleMap/Autocomplete.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32d2dc5c", Component.options)
  } else {
    hotAPI.reload("data-v-32d2dc5c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ((function (x) {
  return x["default"] || x;
})(__webpack_require__(173)));

/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "input",
    _vm._g(
      _vm._b(
        { ref: "input", staticClass: "form-control" },
        "input",
        _vm.$attrs,
        false
      ),
      _vm.$listeners
    )
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-32d2dc5c", module.exports)
  }
}

/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "form-group mb-2" },
        [
          _c("autocomplete", {
            staticClass: "form-control",
            on: { place_changed: _vm.setPlace }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "gmap-map",
        {
          staticStyle: { width: "100%", height: "400px" },
          attrs: { center: _vm.center, zoom: 12 },
          on: { click: _vm.selectMarker }
        },
        _vm._l(_vm.markers, function(m, index) {
          return _c("gmap-marker", {
            key: index,
            attrs: { position: m.position },
            on: { click: _vm.selectMarker }
          })
        }),
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-51b6811f", module.exports)
  }
}

/***/ })

});