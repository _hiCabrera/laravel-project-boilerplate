webpackJsonp([19],{

/***/ 275:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(304)
/* template */
var __vue_template__ = __webpack_require__(305)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/containers/PaginatedSection.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3994db4c", Component.options)
  } else {
    hotAPI.reload("data-v-3994db4c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_utils_mixins__ = __webpack_require__(172);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var Pagination = function Pagination() {
  return __webpack_require__.e/* import() */(22/* duplicate */).then(__webpack_require__.bind(null, 293));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PaginatedSection',
  components: {
    Pagination: Pagination
  },
  computed: {
    displayedTitle: function displayedTitle() {
      return this.searched ? "Search results for \"".concat(this.lastSearch, "\"") : this.title;
    },
    displayedPages: function displayedPages() {
      var pages = [];

      for (var page = 1; page <= (pagination.last_page || pagination.last); page++) {
        pages.push(this.formatPageData(page));
      }

      return pages;
    },
    recordsEmpty: function recordsEmpty() {
      return this.pagination.data && this.pagination.data.length == 0;
    }
  },
  props: {
    title: String,
    fetchLink: String,
    paginatePath: {
      type: String,
      "default": ""
    },
    simplified: {
      type: Boolean,
      "default": false
    },
    minHeight: String
  },
  mixins: [__WEBPACK_IMPORTED_MODULE_0_utils_mixins__["a" /* default */]],
  data: function data() {
    return {
      link: '',
      fetching: true,
      currentPage: 0,
      pagination: {},
      self: {},
      search: "",
      searched: false,
      lastSearch: ""
    };
  },
  methods: {
    getPageClass: function getPageClass(isActive) {
      return {
        'active': isActive,
        'page-item': true
      };
    },
    startSearch: function startSearch() {
      this.searched = !!this.search && this.search.length > 0;
      this.link = this.fetchLink;
      this.fetchData();
    },
    formatPageData: function formatPageData(pageNumber) {
      return {
        active: this.currentPage == pageNumber,
        link: "".concat(this.pagination.path, "?page=").concat(pageNumber),
        name: pageNumber
      };
    },
    fetchData: function fetchData() {
      var _this = this;

      this.fetching = true;
      var params = this.searched ? {
        search: this.lastSearch = this.search
      } : {};
      axios(this.link, {
        params: params
      }).then(function (response) {
        return _this.mapData(response);
      })["catch"](function (error) {
        return _this.fetchFailed(error);
      })["finally"](function () {
        return _this.fetching = false;
      });
    },
    fetchFailed: function fetchFailed(error) {
      this.$emit('fetch-failed', error);
      this.promptErrors(error.response.data.message);
      this.pagination = {
        data: []
      };
    },
    mapData: function mapData(response) {
      this.pagination = this.getPaginationFromPath(response);
      this.currentPage = this.pagination.current_page;
    },
    getPaginationFromPath: function getPaginationFromPath(data) {
      this.paginatePath.split('.').forEach(function (index) {
        return data = data[index];
      });
      return data;
    },
    movePage: function movePage(event) {
      event.preventDefault();
      this.link = event.target.href;
      this.currentPage = parseInt(event.target.dataset.page);
      this.fetchData();
    },
    refreshPage: function refreshPage() {
      if (this.pagination.data.length <= 1) {
        var page = this.pagination.current_page - 1;
        this.link = this.pagination.path + '?page=' + (page <= 0 ? 1 : page);
      }

      this.fetchData();
    },
    pageChanged: function pageChanged(page) {
      event.preventDefault();
      this.link = this.pagination.path + '?page=' + page;
      this.fetchData();
    }
  },
  mounted: function mounted() {
    this.self = this;
    this.link = this.fetchLink;
    this.fetchData();
  }
});

/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm._t("title", null, {
        title: _vm.displayedTitle,
        startSearch: _vm.startSearch,
        search: _vm.search,
        self: _vm.self
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row", style: "min-height: " + _vm.minHeight },
        [
          _vm.fetching
            ? [_vm._t("loading")]
            : _vm.recordsEmpty
            ? [_vm._t("empty")]
            : _vm._l(_vm.pagination.data, function(record) {
                return [
                  _vm._t("body", null, {
                    record: record,
                    mixins: _vm.self,
                    refreshPage: _vm.refreshPage
                  })
                ]
              })
        ],
        2
      ),
      _vm._v(" "),
      [
        (_vm.pagination.last_page || _vm.pagination.last) > 1
          ? _c("pagination", {
              attrs: {
                simplified: _vm.simplified,
                "total-pages": _vm.pagination.last_page || _vm.pagination.last,
                total: _vm.pagination.total,
                "current-page": _vm.pagination.current_page
              },
              on: { pageChanged: _vm.pageChanged }
            })
          : _vm._e()
      ]
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3994db4c", module.exports)
  }
}

/***/ })

});