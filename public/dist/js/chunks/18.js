webpackJsonp([18],{

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(294)
/* template */
var __vue_template__ = __webpack_require__(295)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/containers/cards/Card.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-194ca6be", Component.options)
  } else {
    hotAPI.reload("data-v-194ca6be", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Card',
  components: {},
  computed: {
    hasFooter: function hasFooter() {
      return !!this.$slots.footer;
    },
    hasHeader: function hasHeader() {
      return !!this.$slots.header;
    },
    styleClass: function styleClass() {
      return {
        card: {
          'card': true,
          'bg-secondary': this.gray,
          'bg-white': !this.gray,
          'shadow': true
        },
        cardHeader: {
          'card-header': true,
          'bg-white': true,
          'border-0': true,
          'pb-0': true
        },
        cardBody: {
          'card-body': true
        }
      };
    }
  },
  props: {
    gray: Boolean
  }
});

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { class: _vm.styleClass.card }, [
    _vm.hasHeader
      ? _c("div", { class: _vm.styleClass.cardHeader }, [_vm._t("header")], 2)
      : _vm._e(),
    _vm._v(" "),
    _c("div", { class: _vm.styleClass.cardBody }, [_vm._t("body")], 2),
    _vm._v(" "),
    _vm.hasFooter
      ? _c("div", { staticClass: "card-footer py-4" }, [_vm._t("footer")], 2)
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-194ca6be", module.exports)
  }
}

/***/ })

});