webpackJsonp([0],{

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(297)
/* template */
var __vue_template__ = __webpack_require__(298)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/containers/cards/CardTable.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-00152ae0", Component.options)
  } else {
    hotAPI.reload("data-v-00152ae0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CardTable',
  computed: {
    hasFooter: function hasFooter() {
      return !!this.$slots.footer;
    }
  },
  props: {
    title: String
  }
});

/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card shadow" }, [
    _c("div", { staticClass: "card-header border-0" }, [
      _c("div", { staticClass: "row align-items-center" }, [
        _c("div", { staticClass: "col-sm-12 col-md-4" }, [
          _c("h3", {
            staticClass: "mb-0",
            domProps: { textContent: _vm._s(_vm.title) }
          })
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-sm-12 col-md-8 text-right" },
          [_vm._t("title-buttons")],
          2
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "table-responsive" }, [
      _c("table", { staticClass: "table align-items-center table-flush" }, [
        _c("thead", { staticClass: "thead-light" }, [_vm._t("thead")], 2),
        _vm._v(" "),
        _c("tbody", [_vm._t("tbody")], 2)
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "card-footer py-4" }, [_vm._t("footer")], 2)
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-00152ae0", module.exports)
  }
}

/***/ })

});