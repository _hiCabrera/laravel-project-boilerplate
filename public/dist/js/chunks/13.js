webpackJsonp([13],{

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(334)
/* template */
var __vue_template__ = __webpack_require__(335)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/misc/AvatarBadge.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0aec83d2", Component.options)
  } else {
    hotAPI.reload("data-v-0aec83d2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AvatarBadge',
  computed: {
    styleClass: function styleClass() {
      return {
        frameWrapper: {
          'rounded-circle': this.rounded,
          'avatar-sm': true
        },
        avatarFrame: {
          'rounded-circle': this.rounded,
          'shadow': this.raised,
          'avatar-frame': true
        },
        avatarImage: {
          'avatar-image': true,
          'blur': this.loading
        }
      };
    }
  },
  data: function data() {
    return {
      loading: false,
      frameSize: 36
    };
  },
  methods: {
    resetImageScale: function resetImageScale() {
      var image = this.$refs.preview;
      image.style.minWidth = '';
      image.style.minHeight = '';
      image.style.maxWidth = '';
      image.style.maxHeight = '';
      this.$refs.frame.style.width = this.frameSize + 'px';
      this.$refs.frame.style.height = this.frameSize + 'px';
    },
    rescaleImage: function rescaleImage() {
      this.resetImageScale();
      var image = this.$refs.preview;
      var property = image.naturalWidth > image.naturalHeight ? ['height', 'width', 'minWidth', 'maxHeight'] : ['width', 'height', 'minHeight', 'maxWidth'];
      var length = image.naturalWidth > image.naturalHeight ? [image.naturalWidth, image.naturalHeight] : [image.naturalHeight, image.naturalWidth];
      image.style[property[0]] = this.frameSize + 'px';
      image.style[property[1]] = 'unset';
      image.style[property[2]] = image.style[property[3]] = length[0] * (this.frameSize / length[1]) + 'px';
      this.loading = false;
    }
  },
  props: {
    size: Number,
    rounded: {
      "default": true,
      type: Boolean
    },
    src: {
      "default": '/user-default.png',
      type: String | Object
    },
    raised: {
      type: Boolean,
      "default": false
    }
  },
  mounted: function mounted() {
    var _this = this;

    this.frameSize = this.size > 0 ? this.size : this.frameSize;
    this.resetImageScale();
    this.loading = true;

    this.$refs.preview.onload = function () {
      setTimeout(function () {
        return _this.rescaleImage();
      }, 300);
    };
  }
});

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", { class: _vm.styleClass.frameWrapper }, [
    _c("div", { ref: "frame", class: _vm.styleClass.avatarFrame }, [
      _c("img", {
        ref: "preview",
        class: _vm.styleClass.avatarImage,
        attrs: { src: _vm.src }
      })
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0aec83d2", module.exports)
  }
}

/***/ })

});