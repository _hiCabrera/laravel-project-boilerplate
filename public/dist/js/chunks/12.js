webpackJsonp([12],{

/***/ 284:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(332)
/* template */
var __vue_template__ = __webpack_require__(333)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/misc/FloatingActionButton/ActionButton.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ef78e81e", Component.options)
  } else {
    hotAPI.reload("data-v-ef78e81e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_utils_bus__ = __webpack_require__(174);
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ActionButton',
  computed: {
    isLink: function isLink() {
      return this.href != '#';
    }
  },
  methods: {
    click: function click(event) {
      this.isLink ? window.location = this.href : __WEBPACK_IMPORTED_MODULE_0_src_utils_bus__["a" /* bus */].$emit(this.emit);
    },
    initializeTooltip: function initializeTooltip() {
      $(this.$refs.anchor).tooltip({
        tooltipClass: "tooltip-badge",
        boundary: "window",
        title: this.label,
        content: this.label,
        placement: 'left'
      });
    }
  },
  props: {
    icon: String,
    label: String,
    href: {
      type: String,
      "default": '#'
    },
    emit: String
  },
  mounted: function mounted() {
    if (this.label) {
      this.initializeTooltip();
    }
  }
});

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("li", [
    _c(
      "a",
      {
        ref: "anchor",
        attrs: { href: _vm.href, "data-toggle": "tooltip", title: _vm.label },
        on: {
          click: function($event) {
            $event.stopPropagation()
            return _vm.click($event)
          }
        }
      },
      [_c("i", { class: _vm.icon })]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ef78e81e", module.exports)
  }
}

/***/ })

});