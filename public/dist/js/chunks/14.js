webpackJsonp([14],{

/***/ 276:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(306)
/* template */
var __vue_template__ = __webpack_require__(307)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/forms/inputs/AvatarUploader.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2a7ac492", Component.options)
  } else {
    hotAPI.reload("data-v-2a7ac492", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _props;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AvatarUploader',
  computed: {
    inlineFrameSize: function inlineFrameSize() {
      return {
        width: "".concat(this.frameSize.width, " px"),
        height: "".concat(this.frameSize.height, " px")
      };
    },
    errorMessage: function errorMessage() {
      return Array.isArray(this.error) ? this.error[0] : this.error;
    },
    hasError: function hasError() {
      return this.error && (Array.isArray(this.error) ? !this.error.length : !this.error.trim().length);
    },
    styleClass: function styleClass() {
      return {
        imageFrame: {
          'rounded-circle': this.rounded,
          'shadow': true,
          'avatar-frame': true,
          'frame': true
        },
        avatarImage: {
          'avatar-image': true,
          'blur': this.loading
        }
      };
    }
  },
  data: function data() {
    return {
      loading: false,
      isRequesting: false,
      frameSize: 180,
      error: ""
    };
  },
  methods: {
    clearErrors: function clearErrors() {
      this.error = "";
    },
    imageUploaded: function imageUploaded(event) {
      var _this = this;

      if (!(event.target.files && event.target.files[0])) {
        return;
      }

      this.loading = true;
      var reader = new FileReader();

      reader.onload = function (e) {
        return setTimeout(function () {
          return _this.onImageLoad(e);
        }, 500);
      };

      reader.readAsDataURL(event.target.files[0]);
    },
    onImageLoad: function onImageLoad(e) {
      var _this2 = this;

      setTimeout(function () {
        return _this2.rescaleImage();
      }, 300);
      this.$refs.preview.src = e.target.result;
      this.$emit('input', e.target.result);
      this.$emit('change', e.target.result);
    },
    resetImageScale: function resetImageScale() {
      var image = this.$refs.preview;
      image.style.minWidth = '';
      image.style.minHeight = '';
      image.style.maxWidth = '';
      image.style.maxHeight = '';
    },
    rescaleImage: function rescaleImage() {
      this.resetImageScale();
      var image = this.$refs.preview;
      var property = image.naturalWidth >= image.naturalHeight ? ['height', 'width', 'minHeight', 'minWidth', 'maxWidth'] : ['width', 'height', 'minWidth', 'minHeight', 'maxHeight'];
      var length = image.naturalWidth > image.naturalHeight ? [image.naturalWidth, image.naturalHeight] : [image.naturalHeight, image.naturalWidth];
      image.style[property[0]] = image.style[property[1]] = 'unset';
      image.style[property[2]] = image.style[property[3]] = '100%';
      image.style[property[4]] = length[0] * (this.frameSize[property[0]] / length[1]) + 'px';
      this.loading = false;
    }
  },
  props: (_props = {
    value: {
      "default": '/user-default.png',
      type: String | Object
    },
    size: Number,
    height: Number,
    width: Number
  }, _defineProperty(_props, "size", Number), _defineProperty(_props, "rounded", {
    "default": true,
    type: Boolean
  }), _props),
  watch: {
    value: function value() {
      this.clearErrors();
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    this.frameSize = this.size || this.frameSize;
    this.frameSize = {
      height: this.height || this.size || this.frameSize,
      width: this.width || this.size || this.frameSize
    };
    this.loading = true;

    this.$refs.preview.onload = function () {
      setTimeout(function () {
        return _this3.rescaleImage();
      }, 300);
    };
  }
});

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card card-profile shadow" }, [
    _c("div", { staticClass: "row justify-content-center" }, [
      _c("div", { staticClass: "col-lg-3 order-lg-2" }, [
        _c("div", { staticClass: "card-profile-image" }, [
          _c(
            "div",
            { class: _vm.styleClass.imageFrame, style: _vm.inlineFrameSize },
            [
              _c("img", {
                ref: "preview",
                class: _vm.styleClass.avatarImage,
                attrs: { src: _vm.value }
              })
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "border-0 card-header mt--5 p-0 text-center" }, [
      !_vm.loading || _vm.value == null || _vm.value == ""
        ? _c(
            "button",
            {
              staticClass: "btn btn-default rounded-circle text-center m-0",
              staticStyle: { padding: "5px" },
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  return _vm.$refs.input.click()
                }
              }
            },
            [_c("i", { staticClass: "fa fa-pen p-2" })]
          )
        : _c(
            "button",
            {
              staticClass:
                "btn btn-default rounded-circle text-center disabled m-0",
              staticStyle: { padding: "5px" },
              attrs: { type: "button", disabled: "" }
            },
            [_c("i", { staticClass: "fa fa-spinner fa-spin p-2" })]
          ),
      _vm._v(" "),
      _c("input", {
        ref: "input",
        attrs: { type: "file", hidden: "" },
        on: { change: _vm.imageUploaded }
      })
    ]),
    _vm._v(" "),
    _vm.hasError
      ? _c("div", { staticClass: "mt--3 mb-3 pl-3 pr-3 text-center" }, [
          _c("span", {
            staticClass: "text-danger",
            domProps: { textContent: _vm._s(_vm.errorMessage) }
          })
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2a7ac492", module.exports)
  }
}

/***/ })

});