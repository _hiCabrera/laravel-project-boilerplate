webpackJsonp([9],{

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(345)
/* template */
var __vue_template__ = __webpack_require__(346)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/modals/Modal.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2c7ec46f", Component.options)
  } else {
    hotAPI.reload("data-v-2c7ec46f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Modal",
  computed: {
    id: function id() {
      return "modal-component-".concat(this._uid);
    },
    selector: function selector() {
      return "#".concat(this.id);
    },
    styleClass: function styleClass() {
      return {
        modal: {
          'modal': true,
          'fade': true // 'modal-lg': !this.small,

        },
        modalDialog: {
          'modal-dialog': true,
          'modal-dialog-centered': true,
          'modal-lg': !this.small
        }
      };
    }
  },
  data: function data() {
    return {
      fullscreen: false,
      icon: false,
      show: this.value
    };
  },
  methods: {
    initializeModal: function initializeModal() {
      var _this = this;

      $(this.selector).on('show.bs.modal', function (e) {// this.$nextTick(() => this.$emit("input", this.show = true))
      });
      $(this.selector).on('hide.bs.modal', function (e) {
        _this.$emit('hidden');

        if (!e.target.classList.contains('modal')) {
          e.preventDefault();
          return;
        } // this.$nextTick(() => this.$emit("input", this.show = false))
        // this.$nextTick(() => this.show = false)

      });
      $(this.selector).modal('hide');
    },
    toggleVisibility: function toggleVisibility() {
      $(this.selector).modal(this.show ? 'show' : 'hide');
    }
  },
  props: {
    value: {
      type: Boolean,
      "default": false
    },
    subtitle: String,
    title: String,
    small: {
      type: Boolean,
      "default": true
    }
  },
  watch: {
    show: function show() {
      this.toggleVisibility();
    },
    value: function value(newValue) {
      this.show = newValue;
    }
  },
  mounted: function mounted() {
    this.initializeModal();
  }
});

/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      class: _vm.styleClass.modal,
      attrs: { id: _vm.id, tabindex: "-1", "aria-labelledby": "modal-form" }
    },
    [
      _c(
        "div",
        { class: _vm.styleClass.modalDialog, attrs: { role: "document" } },
        [
          _c("div", { staticClass: "modal-content" }, [
            _c(
              "div",
              { staticClass: "modal-header" },
              [
                _vm.title
                  ? _c("h3", { domProps: { textContent: _vm._s(_vm.title) } })
                  : _vm._t("header"),
                _vm._v(" "),
                _vm._m(0)
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "modal-body pt-0 pb-0" },
              [_vm._t("default")],
              2
            ),
            _vm._v(" "),
            _c("div", { staticClass: "modal-footer" }, [_vm._t("footer")], 2)
          ])
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2c7ec46f", module.exports)
  }
}

/***/ })

});