<?php

return [

    [
        'title'      => 'Dashboard',
        'icon'       => 'fas fa-tachometer-alt',
        'route'      => [
            'name'   => 'pages.dashboard.index',
            'params' => [],
        ],
        'items'      => [],
        'permission' => [],
    ],

    [
        'title'      => 'Users',
        'icon'       => 'fa-users',
        'route'      => [],
        'permission' => [],
        'items'      => [

            [
                'title'      => 'Admin',
                'icon'       => 'fa-user',
                'route'      => [
                    'name'   => 'pages.admins.index',
                    'params' => [],
                ],
                'items'      => [],
                'permission' => [],
            ],

            [
                'title'      => 'Players',
                'icon'       => 'fa-user',
                'route'      => [
                    'name'   => 'pages.players.index',
                    'params' => [],
                ],
                'items'      => [],
                'permission' => [],
            ],

        ],
    ],

];