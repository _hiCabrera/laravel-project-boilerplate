const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'dist/js/')
   .extract([
      'moment',
      'popper.js',
      'jquery',
      'bootstrap',
      'vue',
   ]);

mix.webpackConfig({
   resolve: {
         alias: {
            src: path.resolve(__dirname, "resources/js"),
            components: path.resolve(__dirname, "resources/js/components"),
            models: path.resolve(__dirname, "resources/js/models"),
            store: path.resolve(__dirname, "resources/js/store"),
            pages: path.resolve(__dirname, "resources/js/pages"),
            utils: path.resolve(__dirname, "resources/js/utils"),
         }
   },
   output: {
      path: path.resolve(__dirname, 'public/'),
      chunkFilename: 'dist/js/chunks/[name].js',
   }
});