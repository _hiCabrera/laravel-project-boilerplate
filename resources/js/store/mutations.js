
export const mutations = {
    setPageData(state, payload) {
        state.page = { ... payload }
        return this
    }
}