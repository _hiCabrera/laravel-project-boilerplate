
class Errors {

    constructor() {
        this.clear()    
    }

    has(field) {
        return this.errors.hasOwnProperty(key)
    }

    any() {
        return !!Object.keys(this.errors).length
    }

    clear() {
        this.errors = {}
    }

    get(field) {
        return typeof(this.errors[field]) === "object"
            ? this.errors[field][0]
            : this.errors[field]
    }

    getAll(field) {
        return field 
            ? this.errors[field]
            : this.errors
    }

    record(errors, field) {
        if (!field && typeof(errors) !== "object") {
            throw new Error("Error messages must be an array if not given a field")
        }

        if (field) {
            this.errors[field] = typeof(errors) === "object"
                ? errors
                : [ errors ]
            return
        }
        this.errors = errors
    }
    
}

export default Errors
export { Errors }
