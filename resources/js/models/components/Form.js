import { default as Errors } from './Errors'

class Form {

    constructor(data) {
        this.mapData(data);
        this.isRequesting = false;
        this.submitKeys = [];
        this.errors = new Errors();
    }

    mapData(data) {
        this.originalData = { ... data };
        this.resetData();
    }

    reset() {
        this.resetData();
        // this.clearErrors();
    }

    resetData() {
        Object.keys(this.originalData)
            .forEach(i => this[i] = this.originalData[i]);
    }

    setSubmitKeys(keys) {
        this.submitKeys = Array.isArray(keys) ? keys : [ keys ];
    }

    clearErrors() {
        this.errors.clear();
    }

    getData() {
        let data = {};
        let keys = this.submitKeys.length > 0
            ? this.submitKeys
            : Object.keys(this.originalData);
        keys.forEach(i => data[i] = this[i]);

        return data;
    }

    getHeaders() {
        return {
            'Content-Type': 'multipart/form-data'
        }
    }

    getFormData() {
        let data = this.getData();
        let formData = new FormData();
        Object.keys(data).forEach(key => {
            if (!Array.isArray(data[key])) {
                formData.append(key, data[key]);
                return;
            }
            data[key].forEach(item => formData.append(key + '[]', item));
        });

        return formData;
    }

    send(method, url, multiPart = false) {
        return new Promise((resolve, reject) => {
            this.isRequesting = true;
            this.prepareRequest(method, url, multiPart)
                .then(resolve)
                .catch(error => {
                    // this.errors.record(error)
                    reject(error);
                })
                .finally(() => this.isRequesting = false);
        });
    }

    prepareRequest(method, url, multiPart) {
        let data = multiPart ? this.getFormData() : this.getData();
        let headers = this.getHeaders();

        return multiPart
            ? axios({ method, url, data, headers })
            : axios({ method, url, data })
    }
    
    sendGet(url, multiPart = false) {
        return this.send('get', url);
    }
    
    sendPost(url, multiPart = false) {
        return this.send('post', url, multiPart);
    }

    sendPut(url, multiPart = false) {
        return this.send('put', url);
    }

    sendPatch(url, multiPart = false) {
        return this.send('patch', url);
    }

    sendDelete(url, multiPart = false) {
        return this.send('delete', url);
    }

    static fileListToArray(fileList) {
        let files = [];
        for (const file of fileList) {
            files.push(file);
        }
        return files;
    }

}



export default Form
export { Form }