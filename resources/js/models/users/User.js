import moment from 'moment';
import Form from 'models/components/Form'

class User {

    constructor(data) {
        this.mapData(data)
    }

    mapData(data) {
        let contacts = [];
        data.contacts.forEach(contact => contacts.push(new Form(contact)));
        this.originalData = {
            account: new Form({
                id: data.id,
                email: data.email,
                birthdate: moment(data.birthdate).format('YYYY-MM-DD'),
                display_image: data.display_image,
                firstname: data.firstname,
                civil_status: data.civil_status,
                middlename: data.middlename,
                lastname: data.lastname,
                gender: data.gender,
                suffix: data.suffix,
                status: data.status,
            }),
            contacts: contacts,
            address: new Form({ ... data.address })
        }

        this.account = { ...this.originalData.account }
        this.address = { ...this.originalData.address }
        this.contacts = [ ...this.originalData.contacts ]
    }

    getData() {
        return this.originalData
    }

}

export default User
export { User }