export const Card = () => import('./Card.vue');
export const CardPaginatedTable = () => import('./CardPaginatedTable.vue');
export const CardTable = () => import('./CardTable.vue');
export const CardInfiniteScrollList = () => import('./CardInfiniteScrollList.vue');