export const AvatarBadge = () => import("./AvatarBadge.vue");
export const GoogleMap = () => import("./GoogleMap/GoogleMap.vue");
export const SearchField = () => import("./SearchField.vue");
export const Slider = () => import("./Slider.vue");
export * from './FloatingActionButton';