// export { default as AvatarUploader } from "./AvatarUploader.vue";
// export { default as FormInput } from "./FormInput.vue";
// export { default as FormDatePicker } from "./FormDatePicker.vue";
export const AvatarUploader = () => import("./AvatarUploader.vue");
export const FormInput = () => import("./FormInput.vue");
export const FormDatePicker = () => import("./FormDatePicker.vue");
