
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueSweetalert2 from 'vue-sweetalert2';
import * as VueGoogleMaps from "vue2-google-maps";
import App from './App.vue';

/**
 * Enhance here
 */

import * as components from 'components';
// const requireComponent = require.context('./components/', true, /\.vue$/);

/**
 * Vue SweetAlert 2 usage
 */
const options = {
    confirmButtonColor: '#00c6ff',
    cancelButtonColor: '#d4d4d4',
    cancelTextColor: '#595959'
}
   
window.Vue.use(VueSweetalert2, options)

window.Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyC5A6j9WZhfhfKhQMm8cNSx8SjOA2GO1o0",
        libraries: "places" // necessary for places input
    }
});

// requireComponent.keys().forEach(fileName => {
//     let componentConfig = requireComponent(fileName);
//     componentConfig = componentConfig.default || componentConfig;
//     console.log(componentConfig.name);
    
// });
for (const key in components) {
    window.Vue.component(key, components[key]);
}


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { App }
});
