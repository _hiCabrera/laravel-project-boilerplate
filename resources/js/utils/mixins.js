const mixins = {
    methods: {
        reload(timeout = 0) {
            setTimeout(() => location.reload(), timeout);
        },

        redirect(name, params = {}, timeout = 0) {
            setTimeout(
                () => location.href = route(name, params),
                timeout
            );
        },

        redirectToLink(link, timeout = 0) {
            setTimeout(() => location.href = link, timeout);
        },

        confirm(title, text, type = 'question', buttons = null) {
            buttons = buttons || {confirm: "Yes", cancel: "No"};

            return this.$swal({
                type, title, text,
                confirmButtonText: buttons.confirm,
                cancelButtonText: buttons.cancel,
                showCancelButton: true,
            });
        },

        promptSuccess(message, title = "Success") {
            if (typeof message === "object") {
                title = message.title
                message = message.message
            }
            this.$swal({
                type: "success", title, text: message,
                confirmButtonText: 'Okay',
            });
        },

        promptFormErrors(refs, error, debug = false) {
            if (debug) {
                console.log(error);
            }
            const { response } = error;
            this.promptErrors(response.data.message);
            if (!response.data.errors) {
                return;
            }

            for (const key in response.data.errors) {
                if (!response.data.errors.hasOwnProperty(key)) {
                    return;
                }
                if (!refs[key]) continue;
                refs[key].error = response.data.errors[key];
            }


        },

        promptErrors(message, title = "Something went wrong") {
            if (typeof message === "object") {
                title = message.title
                message = message.message
            }
            this.$swal({
                type: "error", title, text: message,
                confirmButtonText: 'Okay',
            });
        },

        refreshOrRedirect(title, text, route, type = "", button = null) {
            button = button || { confirm: 'Yes', cancel: 'No' };
            return this.$swal({
                    type, title, text,
                    showCancelButton: true,
                    confirmButtonText: button.confirm,
                    cancelButtonText: button.cancel,
                    allowOutsideClick: false,
                })
                .then(response => response.value ? this.reload() : this.redirectToLink(route));
        },

        pad(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        },

        initTooltip(selector = '.tooltiped') {
            $(selector).each(function() {
                $(this).tooltip({
                    tooltipClass: "tooltip-badge",
                    boundary: "window",
                    title: $(this).data('title'),
                    content: $(this).data('title'),
                    placement: 'left',
                })
            });
        }
    }
};

export default mixins;
export { mixins };
