/**
 * [get ]
 * @param  {[type]}    url     [description]
 * @param  {[type]}    params  [description]
 * @param  {...[type]} options [description]
 * @return {[type]}            [Promise]
 */
export function get(url, config, ...options) {
    let request = Object.assign({}, config, { method: "get", url, config });
    return axios(request);
}

export function post(url, config, ...options) {
    let request = Object.assign({}, config, { method: "post", url, config });
    return axios(request);
}

/**
 * [save description]
 * @param  {[type]}    url     [description]
 * @param  {[type]}    config  [ you need to pass a data key, eg, {data:details}]
 * @param  {...[type]} options [description]
 * @return {[type]}            [description]
 */
export function save(url, config, ...options) {
    let request = Object.assign({}, config, { method: "put", url, config });
    return axios(request);
}

export function destroy(url, config, ...options) {
    let request = Object.assign({}, config, { method: "delete", url, config });
    return axios(request);
}

export function send(method, url, config, ...options) {
    let request = Object.assign({}, config, {
        method,
        url,
        config
    });
    return axios(request);
}
