// export { default as LoginForm } from './LoginForm.vue';
export const LoginForm = () => import('./LoginForm.vue');
export const PasswordResetForm = () => import('./PasswordResetForm.vue');
export const ForgotPasswordForm = () => import('./ForgotPasswordForm.vue');