import moment from 'moment';
import Form from 'models/components/Form'

const state = {

    contactForm: {},

    user: {
        account: new Form({
            email: '',
            status: '',
            password: '',
            confirm_password: '',
        }),
        profile: new Form({
            name: '',
            firstname: '',
            middlename: '',
            lastname: '',
            suffix: '',
            birthdate: moment().format('YYYY-MM-DD'),
            gender: '',
            display_image: "user-default.png",
        }),
        contacts: [
            new Form({
                name: '',
                number: '',
            })
        ],
        address: new Form({
            id: null,
            unit_no: '',
            street: '',
            barangay: '',
            town: '',
            province: '',
            zipcode: '',
            lat: 0.0,
            lng: 0.0,
            place_id: 0,
        }),
    }

}

export default state;
export { state };