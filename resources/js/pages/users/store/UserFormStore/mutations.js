import Form from 'models/components/Form';

const mutations = {
    initializeUser(state, payload) {
        state.user = { ...payload }
        return state;
    },

    setAccount(state, account) {
        state.user.account.status = account.status;
        return state;
    },

    setProfile(state, profile) {
        state.user.account.firstname = profile.firstname
        state.user.account.middlename = profile.middlename
        state.user.account.lastname = profile.lastname
        state.user.account.suffix = profile.suffix
        state.user.account.birthdate = profile.birthdate
        state.user.account.gender = profile.gender
        state.user.account.civil_status = profile.civil_status
        return state;
    },

    setAddress(state, address) {
        state.user.address.id = address.id;
        state.user.address.unit_no = address.unit_no;
        state.user.address.street = address.street;
        
        return state;
    },

    setContacts(state, contacts) {
        state.user.contacts = contacts

        return state;
    },

    clearContactForm(state) {
        state.contactForm = {};

        return state;
    },

    initializeCreateContactForm(state) {
        state.contactForm = new Form({
            id: 0, name: '', number: '',
        })

        return state;
    },

    initializeUpdateContactForm(state, contactID) {
        let contact = state.user.contacts
            .filter(contact => contact.id === contactID);
        state.contactForm = contact.length > 0 ? new Form(contact[0]) : {};

        return state;
    },

    addContact(state, contact) {
        state.user.contacts.push(contact);
        
        return state;
    },

    updateContact(state, contact) {
        for (const key in state.user.contacts) {
            if (state.user.contacts[key].id !== contact.id) {
                continue;
            }
            state.user.contacts[key] = contact;
            break;
        }

        return state;
    },

    deleteContact(state, contactId) {
        for (const key in state.user.contacts) {
            if (state.user.contacts[key].id !== contactId) {
                continue;
            }
            state.user.contacts.splice(key, 1);
            break;
        }

        return state;
    },

}


export default mutations;
export { mutations };