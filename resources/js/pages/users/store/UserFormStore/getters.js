
const getters = {

    user(state) {
        return state.user.account;
    },

    address(state) {
        return state.user.address;
    },

    contacts(state) {
        return state.user.contacts;
    },

    profile(state) {
        return state.user.account;
    },

    contactFormData(state) {
        return state.contactForm;
    }

}

export default getters;
export { getters };