import Form from 'models/components/Form';

const actions = {

    saveAccount(context) {
        return new Promise((resolve, reject) => {
            context.getters.user.sendPatch(route(
                'api.backend.users.update-account',
                context.getters.user.id
            ))
                .then(response => resolve(response))
                .catch(error => reject(error));
        });
    },

    saveProfile(context) {
        return new Promise((resolve, reject) => {
            context.getters.profile.sendPatch(route(
                'api.backend.users.update-profile',
                context.getters.user.id
            ))
            .then(response => resolve(response))
            .catch(error => reject(error));
        });
    },

    saveAddress(context) {
        return new Promise((resolve, reject) => {
            context.getters.address.sendPut(route(
                'api.backend.addresses.update', 
                context.getters.address.id
            ))
            .then(response => resolve(response))
            .catch(error => reject(error));
        });
    },

    saveContacts(context) {

    },

    addContact(context) {
        return new Promise((resolve, reject) => {
            context.getters.contactFormData.sendPost(route(
                'api.backend.user-contacts.store',
                context.getters.user.id
            ))
            .then(response => {
                context.commit('addContact', context.getters.contactFormData);
                context.commit('clearContactForm');
                resolve(response);
            })
            .catch(error => reject(error));
        });
    },

    updateContact(context) {
        return new Promise((resolve, reject) => {
            context.getters.contactFormData.sendPut(route(
                'api.backend.user-contacts.update',
                context.getters.contactFormData.id
            ))
            .then(response => {
                context.commit('updateContact', context.getters.contactFormData);
                context.commit('clearContactForm');
                resolve(response);
            })
            .catch(error => reject(error));
        });
    },

    deleteContact(context, id) {
        return new Promise((resolve, reject) => {
            new Form().sendDelete(route('api.backend.user-contacts.destroy', id))
            .then(response => {
                context.commit('deleteContact', id);
                resolve(response);
            })
            .catch(error => reject(error));
        });
    },

}

export default actions;
export { actions };