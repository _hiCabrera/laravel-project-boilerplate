<?php

return [
    'fetch'          => [
        'success' => "Successfully fetched data",
        'failed'  => "Unable to fetch data",
    ],

    'contact'        => [
        'create' => [
            'success' => 'Successfully added contact number',
            'failed'  => 'Failed to add contact number',
        ],
        'update' => [
            'success' => 'Successfully updated user\'s contact number',
            'failed'  => 'Failed to update user\'s contact number',
        ],
        'delete' => [
            'success' => 'Successfully delete user\'s contact number',
            'failed'  => 'Failed to delete user\'s contact number',
        ],
    ],

    'address'        => [
        'update' => [
            'success' => 'Successfully updated address',
            'failed'  => 'Failed to update address',
        ],
    ],

    'admin'          => [
        'create' => [
            'success' => 'Successfully registered an admin',
            'failed'  => 'Failed to register an admin',
        ],
    ],

    'user'           => [

        'address'        => [
            'create' => [
                'success' => 'Successfully saved address',
                'failed'  => 'Failed to saved address',
            ],
            'update' => [
                'success' => 'Successfully saved address',
                'failed'  => 'Failed to saved address',
            ],
        ],  
        'login' => [
            'success' => "User has successfully signed in",
            'failed' => "User failed to sign in",
        ],
        'device'          => [
            'create' => [
                'success' => "Successfully saved user's device",
                'failed'  => "Failed to save user's device",
            ],
        ],
        'contact'         => [
            'create' => [
                'success' => "Successfully saved user's contact",
                'failed'  => "Failed to save user's contact",
            ],
            'update' => [
                'success' => "Successfully updated user's contact",
                'failed'  => "Failed to update user's contact",
            ],
        ],
        'update-image'    => [
            'success' => 'Successfully updated user\'s display image',
            'failed'  => 'Failed to update user\'s display image',
        ],
        'update-password' => [
            'success' => 'Successfully updated user\'s password',
            'failed'  => 'Failed to update user\'s password',
        ],
        'update-account'  => [
            'success' => 'Successfully updated user\'s account information',
            'failed'  => 'Failed to update user\'s account information',
        ],
        'update-profile'  => [
            'success' => 'Successfully updated user\'s profile',
            'failed'  => 'Failed to update user\'s profile',
        ],
        'block' => [
            'success' => "Successfully blocked user",
            'failed' => "Failed to block user",
        ],
        'unblock' => [
            'success' => "Successfully unblock user",
            'failed' => "Failed to unblock user",
        ]
    ],

    'notification'   => [
        'read-all' => [
            'success' => 'Successfully marked notifications as read',
            'failed'  => 'Failed tp mark notifications as read',
        ],
        'read' => [
            'success' => 'Successfully marked notification as read',
            'failed'  => 'Failed tp mark notification as read',
        ],
    ],
    'advertisement'  => [
        'create' => [
            'success' => 'Successfully registered an advertisement',
            'failed'  => 'Failed to register an advertisement',
        ],
        'update' => [
            'success' => 'Successfully updateed an advertisement',
            'failed'  => 'Failed to update an advertisement',
        ],
        'upload-image' => [
            'success' => 'Successfully uploaded image',
            'failed'  => 'Failed to upload image',
        ],
        'delete' => [
            'success' => 'Successfully deleted an advertisement',
            'failed'  => 'Failed to delete an advertisement',
        ],
    ],

    'contact-us' => [
        'create' => [
            'success' => 'Successfully sent message',
            'failed'  => 'Failed to send message',
        ],
        'delete' => [
            'success' => 'Successfully deleted concern',
            'failed'  => 'Failed to delete concern',
        ],
    ],

    'auth' => [
        'send-verification' => [
            'success' => 'Successfully sent verification mail',
            'failed'  => 'Failed to send verification mail',
        ],
        'verify-email' => [
            'success' => 'Successfully verified email',
            'invalid-code' => 'Given code is invalid',
            'failed'  => 'Failed to validate email',
        ],
        'send-reset-link' => [
            'success' => 'Successfully send reset link to your mail',
            'failed'  => 'Failed to send reset link to your mail',
        ],
        'password-reset' => [
            'success' => "Successfully reset your account's password",
            'failed'  => "Failed to reset your account's password",
        ],
    ]
];