@inject('sidebar', 'App\Helpers\SidebarBuilder')

@php $sidebar->setMenuItems(config('sidebar')) @endphp

<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      <!-- Brand -->
        <a class="navbar-brand pt-0" href="#">
            <div class="navbar-brand-img">
                <avatar-badge src="{{ asset('images/brand.png') }}" :size="76"></avatar-badge>
            </div>
            
            <div class="brand-title font-weight-bold">{{ config('app.name') }}</div>
            <div class="brand-sub-title"></div>
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            @include('shared.layouts.includes.header.notifications')
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <avatar-badge src="{{ \Auth::user()->display_image }}"></avatar-badge>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="{{ \Auth::user()->profile_route }}" class="dropdown-item">
                        <i class="fa fa-user"></i>
                        <span>My profile</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('pages.logout') }}" class="dropdown-item">
                        <i class="fa fa-power-off"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
      
      <div class="navbar-collapse collapse" id="sidenav-collapse-main">
        
        <div class="navbar-collapse-header">
            <div class="row">
                <div class="col"></div>
                <div class="col-6 collapse-close">
                    <button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
          </div>
        {{-- <search-field route="#" :dark="false" :navbar="false"></search-field> --}}
        <!-- Collapse -->
        {{-- <h6 class="navbar-heading text-muted">Documentation</h6> --}}
          
        {!! $sidebar->build() !!}
        </div>
    </div>
  </nav>