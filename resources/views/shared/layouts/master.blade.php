<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>@yield('page_title', config('app.name'))</title>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" async>
        <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}">
        @stack('style')
    </head>
    <body class="position-ref full-height bg-secondary">
        <div id="app">
            <app inline-template :user-id="{{ auth()->id() }}">
                <div>
                    @include('shared.layouts.sidebar')
                    <div id="main-content-slot" class="main-content">
                        @include('shared.layouts.header')
                        @yield('content')

                        <div id="floatables" class="__floatable-container">
                            @yield('floatables')
                        </div>

                        @include('shared.layouts.footer')
                    </div>
                </div>
            </app>
        </div>
    </body>
    @routes
    <script src="{{ asset('dist/js/app.js') }}"></script>
    @stack('scripts')
</html>
