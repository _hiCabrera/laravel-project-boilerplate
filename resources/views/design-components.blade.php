<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,600">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" async>
        <link rel="stylesheet" href="https://demos.creative-tim.com/argon-dashboard/assets/css/argon-dashboard.min.css?v=1.1.0">
        <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fafafa;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                /* font-weight: 200; */
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="row mt-5">
            <div class="col-sm-8 offset-sm-2">
                <div class="table-component">
                    <div class="table-toolbar row">
                        <div class="table-toolbar-search col">
                            <i class="fa fa-search table-toolbar-search-icon"></i>
                            <input class="table-toolbar-search-field" type="text" placeholder="Search">
                        </div>
                        <div class="table-toolbar-items col-auto">
                            <button class="table-toolbar-item">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <h2 class="table-title">Users</h2>
                    <div class="table-wrapper">
                        <table>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cabrera, Ian C.</td>
                                    <td>Delete</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="table-pagination">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class= "page-link">
                                    <i class="fas fa-step-backward"></i>
                                </a>
                            </li>
                            @foreach ([1,2,3,4,5] as $item)
                                <li class="page-item {{ $item == 2 ? 'active' : "" }}">
                                    <a class= "page-link">
                                        {{ $item }}
                                    </a>
                                </li>
                            @endforeach
                            <li class="page-item">
                                <a class= "page-link">
                                    <i class="fas fa-step-forward"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="app">
                    <data-table
                        title="Users"
                        fetch-link="{{ route('api.backend.users.index') }}"
                        paginate-path="data.result.users"
                        selectable
                        >
                        <template slot="filters" slot-scope="props">
                            <form-date-picker label="Created Date" alternative format="MMMM DD, YYYY" v-model="props.filters.created_at"></form-date-picker>
                        </template>
                        <template slot="thead">
                            <th>ID</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Gender</th>
                            <th>Date Created</th>
                        </template>
                        <template slot="tbody"  slot-scope="props">
                            <td v-text="props.record.id"></td>
                            <td v-text="props.record.code"></td>
                            <td v-text="props.record.name"></td>
                            <td v-text="props.record.status"></td>
                            <td v-text="props.record.gender"></td>
                            <td v-text="props.record.created_at"></td>
                        </template>
                    </data-table>
                </div>
            </div>
        </div>
    </body>
    <script src="{{ asset('dist/js/manifest.js') }}"></script>
    <script src="{{ asset('dist/js/vendor.js') }}"></script>
    <script src="{{ asset('dist/js/app.js') }}"></script>
</html>
