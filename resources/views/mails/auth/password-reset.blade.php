@component('mail::message')
    <table>
        <tbody>
            <tr><td>PASSWORD RESET</td></tr>
            <tr><td>Dear User,</td></tr>
            <tr>
                <td>
                    <section>
                        <small>
                            <p>A password reset has been requested for your account in MOBILESYS</p>
                            <p>
                                If you did not make this request, you can safely ignore this email. A password reset request can be made by anyone, and it does not indicate that your account is in any danger of being accessed by someone else.
                            </p>
                            <p>If you do actually want to reset your password, visit this link:</p>
                        </small>
                    </section>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="verification" style="width: 100%;text -align: center;">
                        <tbody>
                            <tr>
                                <td style="font-size: 16px;">
                                    <a href="{{ route('pages.auth.password-reset', ['token' => $passwordReset->token]) }}">
                                        {{ route('pages.auth.password-reset', ['token' => $passwordReset->token]) }}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <section>
                                        <small>
                                            This link will expire on {{ $passwordReset->expired_at->format('F m, Y h:i a') }}
                                        </small>
                                        <small>Other Terms and Conditions apply.</small>
                                    </section>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
@endcomponent